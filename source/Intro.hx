package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.addons.display.FlxBackdrop;
import flixel.text.FlxText;
import flixel.util.FlxColor;
import sprites.CustomButton;
import sprites.Item;
import sprites.LocalText;

class Intro extends FlxState
{
	override public function create()
	{
		super.create();

		this.bgColor = FlxColor.WHITE;

		var backdrop = new FlxBackdrop(AssetPaths.backdrop__png);
		backdrop.alpha = 0.5;
		add(backdrop);

		add((new FlxSprite(20, 20)).makeGraphic(FlxG.width - 40, FlxG.height - 40, FlxColor.fromString("ffe06f8b")));
		add((new FlxSprite(24, 24)).makeGraphic(FlxG.width - 48, FlxG.height - 48, FlxColor.WHITE));

		var stateWidth = FlxG.width;
		var stateHeight = FlxG.height;
		var description = new LocalText(40, 40, stateWidth - 80, "Fat Larry has found a new All-U-Can-Eat buffet.
But this place is scetchy a-f.
The floor might crack if You eat too much.
But be aware - there are hungry children hiding there - they will try to steal Your food.
Loose all Your food and Larry will go home.






Food has weight (dahh!)", 20);

		add(description);

		var allItems = ItemType.createAll();
		var xOffset = Math.floor((stateWidth - allItems.length * 160) / 2);
		var yOffset = Math.floor(stateHeight * 0.7);
		for (i in 0...allItems.length)
		{
			add(new LocalText(xOffset + i * 160, yOffset - 80, 120, allItems[i].getName(), 16));
			var item = new Item(xOffset + i * 160 + 48, yOffset, allItems[i]);
			item.scale.x = 4;
			item.scale.y = 4;
			add(item);
			add(new LocalText(xOffset + i * 160, yOffset + 80, 120, Std.string(item.weight), 16));
		}

		var buttonWidth = 360;
		var button = new CustomButton((stateWidth - buttonWidth) / 2, FlxG.height - 80, clickPlay, "Start Eating", buttonWidth);
		add(button);
	}

	override public function update(elapsed:Float)
	{
		super.update(elapsed);
	}

	private function clickPlay()
	{
		FlxG.switchState(new PlayState());
	}
}
