package;

import GameOver.GameOverReason;
import flixel.FlxG;
import flixel.FlxState;
import flixel.addons.display.FlxBackdrop;
import flixel.effects.FlxFlicker;
import flixel.group.FlxGroup;
import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import flixel.system.FlxSound;
import flixel.tile.FlxTilemap;
import flixel.util.FlxColor;
import flixel.util.FlxStringUtil;
import generate.Generator;
import sprites.EvilChild;
import sprites.Item;
import sprites.Player;

// Importing the flixelighting classes
class PlayState extends FlxState
{
	var walls:FlxTilemap;
	var backdrop:FlxBackdrop;
	var player:Player;
	var items:FlxTypedGroup<Item>;
	var evilChildren:FlxTypedGroup<EvilChild>;

	var eatenSound:FlxSound;
	var lostSound:FlxSound;
	var nextLevelSound:FlxSound;
	var prevLevelSound:FlxSound;

	var inventory:Array<Item> = new Array();
	var itemsEaten:Map<ItemType, Int> = new Map<ItemType, Int>();
	var weight:Int = 0;

	var hud:HUD;

	var currentLevel:Int = 1;

	var maxLevel:Int = 1;
	var maxLevelTries:Int = 1;
	var maxWeight:Int = 0;

	function maxWeightForLevel(level):Int
	{
		return Math.floor(Math.pow(level + 1, 2) * 10);
	}

	function levelSize(level:Int):Int
	{
		return Math.floor(Math.min(100, Math.max((level + 1) * 5, 20)));
	}

	override public function create()
	{
		super.create();

		eatenSound = FlxG.sound.load(AssetPaths.pickup__ogg);
		lostSound = FlxG.sound.load(AssetPaths.lost__ogg);
		nextLevelSound = FlxG.sound.load(AssetPaths.nextlevel__ogg);
		prevLevelSound = FlxG.sound.load(AssetPaths.gameover__ogg);

		// init itemsEaten
		var allItems = ItemType.createAll();
		for (i in 0...allItems.length)
		{
			itemsEaten.set(allItems[i], 0);
		}

		backdrop = new FlxBackdrop(AssetPaths.backdrop__png);
		backdrop.alpha = 0.3;
		add(backdrop);

		loadLevel(1);
	}

	override public function update(elapsed:Float)
	{
		super.update(elapsed);
		FlxG.collide(player, walls);
		FlxG.collide(player, items, playerItemPickup);
		FlxG.collide(player, evilChildren, playerItemLost);
		FlxG.collide(evilChildren, walls);
		if (evilChildren != null)
		{
			evilChildren.forEachAlive(checkEnemyVision);
		}
	}

	function checkEnemyVision(enemy:EvilChild)
	{
		if (FlxMath.distanceBetween(enemy, player) <= 300)
		{
			var ep = enemy.getMidpoint();
			var pp = player.getMidpoint();
			var path = walls.findPath(ep, pp);
			if (path != null && path.length > 0)
			{
				enemy.seesPlayer = true;
				if (path.length > 2)
				{
					enemy.playerPosition = path[1];
				}
				else
				{
					enemy.playerPosition = pp;
				}
			}
			else
			{
				enemy.seesPlayer = false;
			}
		}
		else
		{
			enemy.seesPlayer = false;
		}
	}

	function playerItemPickup(p:Player, i:Item)
	{
		if (!inventory.contains(i))
		{
			eatenSound.play(true);
			inventory.push(i);
			itemsEaten.set(i.type, itemsEaten.get(i.type) + 1);
			hud.setItemCount(i.type, itemsEaten.get(i.type));
			weight = weight + i.weight;
			hud.setWeight(weight);
			i.kill();
			items.remove(i);
		}

		if (weight > maxWeight)
		{
			maxWeight = weight;
		}
		if (weight >= maxWeightForLevel(currentLevel))
		{
			// Drop the player 1 level down
			loadLevel(currentLevel + 1);
		}
		else if (items.countLiving() <= 0)
		{
			gameOver(NO_MORE);
		}
	}

	function playerItemLost(p:Player, c:EvilChild)
	{
		if (!c.canHurt)
		{
			return;
		}
		if (inventory.length >= 1)
		{
			lostSound.play(true);
			FlxFlicker.flicker(player);
			var lostItem = inventory.pop();
			itemsEaten.set(lostItem.type, itemsEaten.get(lostItem.type) - 1);
			hud.setItemCount(lostItem.type, itemsEaten.get(lostItem.type));
			weight = weight - lostItem.weight;
			hud.setWeight(weight);
			c.setInactive();

			if (currentLevel != 1 && weight < maxWeightForLevel(currentLevel - 1))
			{
				// Push player one level up
				loadLevel(currentLevel - 1);
			}
		}
		else
		{
			gameOver(LOST_ALL);
		}
	}

	function loadLevel(x:Int)
	{
		FlxG.sound.music.pause();

		if (x == 31)
		{
			FlxG.switchState(new YouWon(maxLevel, maxWeight, itemsEaten));
		}

		if (player != null)
		{
			if (x > currentLevel)
			{
				player.animation.play("fall", true);
				player.animation.finishCallback = loadLevelForReal;
			}
			else if (x < currentLevel)
			{
				FlxFlicker.stopFlickering(player);
				player.animation.play("jump", true);
				player.animation.finishCallback = loadLevelForReal;
			}
		}
		if (evilChildren != null)
		{
			evilChildren.forEach(function(c)
			{
				c.active = false;
			});
		}
		// Max level retry, limit to 3
		if (currentLevel == maxLevel && currentLevel - x == 1)
		{
			maxLevelTries = maxLevelTries + 1;
			if (maxLevelTries > 3)
			{
				gameOver(MAX_RETRIES);
			}
		}

		currentLevel = x;
		if (currentLevel > maxLevel)
		{
			maxLevel = currentLevel;
			maxLevelTries = 1;
		}
		if (walls == null)
		{
			loadLevelForReal("fall");
		}
	}

	function loadLevelForReal(lvl:String)
	{
		var lvls = ["fall", "jump"];
		if (!lvls.contains(lvl))
		{
			return;
		}
		FlxG.camera.fade(FlxColor.BLACK, 0.33, false, function()
		{
			if (walls != null)
			{
				remove(walls);
			}
			if (items != null)
			{
				remove(items);
			}
			if (evilChildren != null)
			{
				remove(evilChildren);
			}

			var generator = new Generator(levelSize(currentLevel), levelSize(currentLevel));

			walls = new FlxTilemap();
			walls.loadMapFromCSV(FlxStringUtil.bitmapToCSV(generator.mapData), AssetPaths.walls__png, 64, 64);

			walls.follow();
			walls.solid = true;
			add(walls);

			items = new FlxTypedGroup<Item>();
			add(items);

			evilChildren = new FlxTypedGroup<EvilChild>();
			add(evilChildren);

			if (player == null)
			{
				player = new Player();
				add(player);
				FlxG.camera.follow(player, TOPDOWN, 1);
			}

			if (hud == null)
			{
				hud = new HUD();
				add(hud);
			}
			hud.setLevel(currentLevel);
			hud.setMaxWeight(maxWeightForLevel(currentLevel));

			loadEntities();

			player.scale.x = (currentLevel + 10) / 10;
			player.animation.play("idle");
			FlxG.camera.focusOn(player.getPosition());
			FlxG.camera.fade(FlxColor.BLACK, 0.33, true, function()
			{
				player.active = true;
				FlxG.sound.music.play(true);
			});
		});
	}

	function loadEntities()
	{
		var emptyTiles:Array<FlxPoint> = walls.getTileCoords(0, false);

		var randomEmptyTile:FlxPoint;

		randomEmptyTile = emptyTiles[FlxG.random.int(0, emptyTiles.length - 1)];
		emptyTiles.remove(randomEmptyTile);

		player.setPosition(randomEmptyTile.x, randomEmptyTile.y);

		var i = 0;
		while (i < currentLevel * 3)
		{
			// Dont put anything too close to player
			do
			{
				randomEmptyTile = emptyTiles[FlxG.random.int(0, emptyTiles.length - 1)];
			}
			while (FlxMath.distanceToPoint(player, randomEmptyTile) < 400);

			emptyTiles.remove(randomEmptyTile);

			var newItem = new Item(randomEmptyTile.x, randomEmptyTile.y);
			items.add(newItem);
			i++;
		}

		i = 0;
		// Dont include enemies on the first level
		while (i < currentLevel)
		{
			// Dont put anything too close to player
			do
			{
				randomEmptyTile = emptyTiles[FlxG.random.int(0, emptyTiles.length - 1)];
			}
			while (FlxMath.distanceToPoint(player, randomEmptyTile) < 400);
			emptyTiles.remove(randomEmptyTile);

			var newItem = new EvilChild(randomEmptyTile.x, randomEmptyTile.y);
			evilChildren.add(newItem);
			i++;
		}
	}

	function gameOver(reason:GameOverReason)
	{
		FlxG.switchState(new GameOver(maxLevel, maxWeight, itemsEaten, reason));
	}
}
