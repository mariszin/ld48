package;

import flixel.FlxG;
import flixel.FlxState;
import flixel.addons.display.FlxBackdrop;
import flixel.util.FlxColor;
import sprites.CustomButton;
import sprites.Item;
import sprites.LocalText;

class MenuState extends FlxState
{
	var playButton:CustomButton;

	override public function create()
	{
		super.create();

		FlxG.sound.cacheAll();

		FlxG.sound.playMusic(AssetPaths.theme__ogg);

		this.bgColor = FlxColor.WHITE;

		var backdrop = new FlxBackdrop(AssetPaths.backdrop__png);
		backdrop.alpha = 0.5;
		add(backdrop);

		add(new LocalText(0, 0, FlxG.width, "Fat Larry", 128));
		add(new LocalText(0, 200, FlxG.width, "At the All-U-Can-Eat Buffet", 32));

		add(new LocalText(0, FlxG.height - 100, FlxG.width, "Created By mariszin | Ludum Dare 48 | 2021", 24));

		// var splash = new FlxBackdrop(AssetPaths.splash__png);
		// // splash.loadGraphic(, false, FlxG.width, FlxG.height);
		// add(splash);

		playButton = new CustomButton(0, 0, clickPlay, "Play");
		playButton.screenCenter();
		add(playButton);

		var stateWidth = FlxG.width;
		var stateHeight = FlxG.height;
		var allItems = ItemType.createAll();
		var xOffset = Math.floor((stateWidth - allItems.length * 128) / 2);
		var yOffset = stateHeight - 240;
		for (i in 0...allItems.length)
		{
			var item = new Item(xOffset + i * 128, yOffset, allItems[i]);
			item.scale.x = 4;
			item.scale.y = 4;
			add(item);
		}
	}

	override public function update(elapsed:Float)
	{
		super.update(elapsed);
	}

	private function clickPlay()
	{
		if (FlxG.sound.music != null)
		{
			FlxG.sound.music.pause();
		}
		FlxG.switchState(new Intro());
	}
}
