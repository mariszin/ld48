package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.addons.display.FlxBackdrop;
import flixel.group.FlxGroup;
import flixel.system.FlxSound;
import flixel.text.FlxText;
import flixel.util.FlxColor;
import sprites.CustomButton;
import sprites.Item;
import sprites.LocalText;

class GameOver extends FlxState
{
	var maxLevel:Int;
	var maxWeight:Int;
	var reason:GameOverReason;
	var itemsLeft:Map<ItemType, Int>;
	var gameOverMusic:FlxSound;

	public function new(maxLevel:Int, maxWeight:Int, itemsLeft:Map<ItemType, Int>, reason:GameOverReason)
	{
		super();
		this.maxLevel = maxLevel;
		this.maxWeight = maxWeight;
		this.itemsLeft = itemsLeft;
		this.reason = reason;
	}

	override public function create()
	{
		super.create();
		FlxG.sound.music.pause();
		gameOverMusic = FlxG.sound.load(AssetPaths.gameover__ogg);
		gameOverMusic.play(true);

		this.bgColor = FlxColor.WHITE;

		var backdrop = new FlxBackdrop(AssetPaths.backdrop__png);
		backdrop.alpha = 0.5;
		add(backdrop);

		add((new FlxSprite(20, 20)).makeGraphic(FlxG.width - 40, FlxG.height - 40, FlxColor.fromString("ffe06f8b")));
		add((new FlxSprite(24, 24)).makeGraphic(FlxG.width - 48, FlxG.height - 48, FlxColor.WHITE));

		var stateWidth = FlxG.width;
		var stateHeight = FlxG.height;

		switch reason
		{
			case LOST_ALL:
				add(new LocalText(0, 80, FlxG.width, "The children have taken all Your food.", 32));
			case NO_MORE:
				add(new LocalText(0, 80, FlxG.width, "There is no more food left.", 32));
			case MAX_RETRIES:
				add(new LocalText(0, 80, FlxG.width, "You can try the same thing just so many times.", 32));
		}

		add(new LocalText(0, 120, FlxG.width, "You must leave. Now!", 16));

		add(new LocalText(0, 160, FlxG.width, "Max Level: " + maxLevel, 24));
		add(new LocalText(0, 200, FlxG.width, "Max Weight: " + maxWeight, 24));
		add(new LocalText(0, 280, FlxG.width, "Food Left", 24));

		var allItems = ItemType.createAll();
		var xOffset = Math.floor((stateWidth - allItems.length * 160) / 2);
		var yOffset = Math.floor(stateHeight * 0.6);
		for (i in 0...allItems.length)
		{
			var itemText = new LocalText(xOffset + i * 160, yOffset - 80, 120, allItems[i].getName(), 16);
			add(itemText);
			var item = new Item(xOffset + i * 160 + 48, yOffset, allItems[i]);
			item.scale.x = 4;
			item.scale.y = 4;
			add(item);
			var itemWeight = new LocalText(xOffset + i * 160, yOffset + 80, 120, Std.string(itemsLeft[allItems[i]]), 16);
			add(itemWeight);
		}

		var buttonWidth = 360;
		var button = new CustomButton((stateWidth - buttonWidth) / 2, FlxG.height - 80, clickPlay, "Start Eating", buttonWidth);
		add(button);
	}

	override public function update(elapsed:Float)
	{
		super.update(elapsed);
	}

	private function clickPlay()
	{
		FlxG.switchState(new PlayState());
	}
}

enum GameOverReason
{
	LOST_ALL;
	NO_MORE;
	MAX_RETRIES;
}
