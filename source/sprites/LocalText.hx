package sprites;

import flixel.text.FlxText;
import flixel.util.FlxColor;

class LocalText extends FlxText
{
	override public function new(X:Float = 0, Y:Float = 0, FieldWidth:Float = 0, ?Text:String, Size:Int = 8, EmbeddedFont:Bool = true)
	{
		super(X, Y, FieldWidth, Text, Size, EmbeddedFont);
		color = FlxColor.fromString("#ffe06f8b");
		// borderColor = FlxColor.BLACK;
		// borderStyle = OUTLINE_FAST;
		alignment = CENTER;
	}
}
