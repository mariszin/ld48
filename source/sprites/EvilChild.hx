package sprites;

import flixel.FlxSprite;
import flixel.effects.FlxFlicker;
import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import flixel.math.FlxVelocity;

class EvilChild extends FlxSprite
{
	public var canHurt:Bool = true;

	var brain:FSM;
	var idleTimer:Float;
	var moveDirection:Float;

	static inline var SPEED = 250;

	public var seesPlayer:Bool;
	public var playerPosition:FlxPoint;

	public function new(x:Float = 0, y:Float = 0)
	{
		super(x, y);

		loadGraphic(AssetPaths.evil_child__png, true, 22, 48);
		animation.add("move", [1, 2, 3, 4, 5, 6, 5, 4, 3, 2], 12, true);
		animation.add("idle", [0], 1, false);

		setSize(22, 48);

		drag.x = drag.y = 1200;

		brain = new FSM(idle);
		idleTimer = 0;
		playerPosition = FlxPoint.get();
	}

	override function update(elapsed:Float)
	{
		brain.update(elapsed);
		super.update(elapsed);
	}

	public function setInactive()
	{
		canHurt = false;
		alpha = 0.75;
		FlxFlicker.flicker(this, 3, 0.1, true, true, function(_)
		{
			canHurt = true;
			alpha = 1;
		});
	}

	function idle(elapsed:Float)
	{
		animation.play("idle");
		if (seesPlayer)
		{
			brain.activeState = chase;
		}
		// Should add some idle movement
		// else if (idleTimer <= 0)
		// {
		// 	if (FlxG.random.bool(1))
		// 	{
		// 		moveDirection = -1;
		// 		velocity.x = velocity.y = 0;
		// 	}
		// 	else
		// 	{
		// 		moveDirection = FlxG.random.int(0, 8) * 45;

		// 		velocity.set(SPEED * 0.5, 0);
		// 		velocity.rotate(FlxPoint.weak(), moveDirection);
		// 	}
		// 	idleTimer = FlxG.random.int(1, 4);
		// }
		// else
		// 	idleTimer -= elapsed;
	}

	function chase(elapsed:Float)
	{
		if (!canHurt)
		{
			animation.play("idle");
			return;
		}
		if (!seesPlayer)
		{
			brain.activeState = idle;
		}
		else if (seesPlayer && playerPosition != null)
		{
			animation.play("move");

			FlxVelocity.moveTowardsPoint(this, playerPosition, SPEED);
		}
	}
}
