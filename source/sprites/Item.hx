package sprites;

import AssetPaths;
import flixel.FlxG;
import flixel.FlxSprite;

class Item extends FlxSprite
{
	public var type:ItemType;
	public var weight:Int = 0;

	public function new(x:Float = 0, y:Float = 0, type:ItemType = null)
	{
		super(x, y);

		var allItems = ItemType.createAll();

		if (type != null)
		{
			this.type = type;
		}
		else
		{
			this.type = allItems[FlxG.random.int(0, allItems.length - 1)];
		}

		loadGraphic(AssetPaths.items__png, true, 32, 32);
		for (i in 0...allItems.length)
		{
			animation.add(allItems[i].getName(), [i], 1, false);
		}

		switch this.type
		{
			case Fries:
				animation.play(Fries.getName());
				weight = 20;
			case Chicken:
				animation.play(Chicken.getName());
				weight = 25;
			case Burger:
				animation.play(Burger.getName());
				weight = 35;
			case OnionRings:
				animation.play(OnionRings.getName());
				weight = 15;
			case IceCream:
				animation.play(IceCream.getName());
				weight = 45;
			case Pica:
				animation.play(Pica.getName());
				weight = 75;
		}
	}

	override function update(elapsed:Float)
	{
		super.update(elapsed);
	}
}

enum ItemType
{
	Fries;
	Chicken;
	Burger;
	OnionRings;
	IceCream;
	Pica;
}
