package sprites;

import flixel.addons.ui.FlxButtonPlus;
import flixel.util.FlxColor;

class CustomButton extends FlxButtonPlus
{
	public function new(X:Float = 0, Y:Float = 0, ?Callback:() -> Void, ?Label:String, Width:Int = 160, Height:Int = 48)
	{
		super(X, Y, Callback, Label, Width, Height);
		updateInactiveButtonColors([FlxColor.fromString("#ffe06f8b")]);
		updateActiveButtonColors([FlxColor.fromString("#fed84860")]);
		textNormal.size = 32;
		textHighlight.size = 32;
	}

	override function update(elapsed:Float)
	{
		super.update(elapsed);
	}
}
