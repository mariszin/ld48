package sprites;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.math.FlxPoint;

class Player extends FlxSprite
{
	static inline var SPEED = 400;

	public function new(x:Float = 0, y:Float = 0)
	{
		super(x, y);

		loadGraphic(AssetPaths.player__png, true, 64, 64);

		animation.add("idle", [0], true);
		animation.add("walk", [0, 1, 0, 2], 4, true);
		animation.add("fall", [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 12, 12], 8, false);
		animation.add("jump", [15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 14, 14, 14], 8, false);

		animation.play("idle");
		setSize(20, 56);
		offset.set(24, 6);

		drag.x = drag.y = 1200;
	}

	function updateMovement()
	{
		var up:Bool = false;
		var down:Bool = false;
		var left:Bool = false;
		var right:Bool = false;

		up = FlxG.keys.anyPressed([UP, W]);
		down = FlxG.keys.anyPressed([DOWN, S]);
		left = FlxG.keys.anyPressed([LEFT, A]);
		right = FlxG.keys.anyPressed([RIGHT, D]);

		if (up && down)
			up = down = false;
		if (left && right)
			left = right = false;

		if (up || down || left || right)
		{
			var newAngle:Float = 0;
			if (up)
			{
				newAngle = -90;
				if (left)
					newAngle -= 45;
				else if (right)
					newAngle += 45;
				facing = FlxObject.UP;
			}
			else if (down)
			{
				newAngle = 90;
				if (left)
					newAngle += 45;
				else if (right)
					newAngle -= 45;
				facing = FlxObject.DOWN;
			}
			else if (left)
			{
				newAngle = 180;
				facing = FlxObject.LEFT;
			}
			else if (right)
			{
				newAngle = 0;
				facing = FlxObject.RIGHT;
			}

			animation.play("walk");
			velocity.set(SPEED, 0);
			velocity.rotate(FlxPoint.weak(0, 0), newAngle);
		}
		else
		{
			animation.play("idle");
		}
	}

	override function update(elapsed:Float)
	{
		var movableAnimation = ["idle", "walk"];
		if (movableAnimation.contains(animation.name))
		{
			updateMovement();
		}
		super.update(elapsed);
	}
}
