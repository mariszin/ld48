package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxGroup.FlxTypedGroup;
import flixel.text.FlxText;
import flixel.util.FlxColor;
import sprites.Item;

using flixel.util.FlxSpriteUtil;

class HUD extends FlxTypedGroup<FlxSprite>
{
	var weightCounter:FlxText;
	var levelCounter:FlxText;

	var weight:Int = 0;
	var maxWeight:Int = 0;
	var level:Int = 1;

	var allItems:Array<ItemType>;
	var itemTexts:Map<ItemType, FlxText> = new Map<ItemType, FlxText>();

	public function new()
	{
		super();

		var backgroundBorder = new FlxSprite().makeGraphic(FlxG.width, 44, FlxColor.WHITE);
		var background = new FlxSprite(2, 2).makeGraphic(FlxG.width - 4, 40, FlxColor.BLACK);

		weightCounter = new FlxText(16, 8, 480, "Weight: 0", 24);
		weightCounter.setBorderStyle(SHADOW, FlxColor.GRAY, 1, 1);
		levelCounter = new FlxText(FlxG.width - 160, 8, 160, "Level: 1", 24);
		levelCounter.setBorderStyle(SHADOW, FlxColor.GRAY, 1, 1);

		add(backgroundBorder);
		add(background);
		add(weightCounter);
		add(levelCounter);

		allItems = ItemType.createAll();

		var xOffset:Int = Math.floor((FlxG.width - allItems.length * 76) / 2);

		for (i in 0...allItems.length)
		{
			var sprite = new Item(xOffset + i * 76, 4, allItems[i]);
			var text = new FlxText(xOffset + i * 76 + 40, 8, 36, "0", 24);
			itemTexts.set(allItems[i], text);
			add(sprite);
			add(text);
		}

		forEach(function(sprite) sprite.scrollFactor.set(0, 0));
	}

	public function setWeight(weight:Int)
	{
		this.weight = weight;
		redrawHud();
	}

	public function setMaxWeight(maxWeight:Int)
	{
		this.maxWeight = maxWeight;
		redrawHud();
	}

	public function setLevel(level:Int)
	{
		this.level = level;
		redrawHud();
	}

	public function setItemCount(item:ItemType, count:Int)
	{
		itemTexts.get(item).text = "" + count;
	}

	function redrawHud()
	{
		weightCounter.text = "Weight: " + weight + " / " + maxWeight;
		levelCounter.text = "Level: " + level;
	}
}
